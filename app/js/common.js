$( document ).ready(function() {

	var hamburger = document.querySelector('.hamburger');
	hamburger.onclick = function() {
		this.classList.toggle("is-active");
	};

	
	// Start left menu
	var 
	arrSeconderySrenHeight  = {},
	arrLinkMenuHeight				= {},
	colLinkMenu				 			= 9,
	arrLinkMenu_1						= document.querySelectorAll('.left-menu-1 a'),
	arrLinkMenu_2						= document.querySelectorAll('.left-menu-2 a'),
	arrLinkMenu_3						= document.querySelectorAll('.left-menu-3 a');


	getHeigtBlocks();

	function getHeigtBlocks() {
		for (var i = 1; i <= 3 ; i++) {
			var topDiv 			= $('.secondery-sren-'+i).offset().top;
			var bottomDiv 	= $('.secondery-sren-'+i).outerHeight();
			arrSeconderySrenHeight['secondery-sren-'+i+'-top'] 	 = 	Math.round(topDiv);
			arrSeconderySrenHeight['secondery-sren-'+i+'-bottom'] = 	Math.round(topDiv + bottomDiv);
		};	
		for (var i = 1; i <= colLinkMenu; i++) {
			arrLinkMenuHeight['title_'+i] =  $('#title-'+i).offset().top;	
		};
	};

	function  ScrollMenu(option) {
		var
		startScroll = option.startScroll ,
		stopScroll	=	option.stopScroll,
		nameMenu		= option.nameMenu,
		heightMenu	= 980,
		windowScoll = 0;


		$(window).scroll(function(){
			windowScoll 	= document.body.scrollTop || document.documentElement.scrollTop;

			if ( windowScoll > startScroll ) { 	//start scroll
				addClass();
				setStyleTop('0');
			}
			if ( windowScoll < startScroll) { 	//stop scroll
				removeClass();
			}
			if ( windowScoll > (stopScroll - heightMenu)  ) { 	//fixed bottom 
				setStyleTop();
			}
			if(true){
				activLink('1')
			}
		});



		function removeClass() {
			nameMenu.classList.remove('fixed-menu')
		};
		function addClass() {
			nameMenu.classList.add('fixed-menu')
		};
		function setStyleTop( top ) {
			var count;
			( top ) ? count = top : count = (stopScroll - windowScoll - heightMenu );
			nameMenu.setAttribute('style', 'top: '+count+'px')
		}
		function activLink( arg ) {
			//console.log( arg)
			//arrLinkMenu_ + arg[2].classList.add('activ')
		}
	};

	startMenu(); 

	$(window).resize( function() {
		getHeigtBlocks();
		startMenu();
	})	

	function startMenu() {
		var menu_1 = ScrollMenu({		
			'startScroll'	: arrSeconderySrenHeight['secondery-sren-1-bottom'],
			'stopScroll'	: arrSeconderySrenHeight['secondery-sren-2-top'],
			'nameMenu'		: document.querySelectorAll( '.left-menu')[0]
		});
		var menu_2 = ScrollMenu({		
			'startScroll'	: arrSeconderySrenHeight['secondery-sren-2-bottom'],
			'stopScroll'	: arrSeconderySrenHeight['secondery-sren-3-top'],
			'nameMenu'		: document.querySelectorAll( '.left-menu')[1]
		});
		var menu_3 = ScrollMenu({		
			'startScroll'	: arrSeconderySrenHeight['secondery-sren-3-bottom'],
			'stopScroll'	: $(document).height(),
			'nameMenu'		: document.querySelectorAll( '.left-menu')[2]
		});
	};
// End left menu

$(".left-menu").on("click","a", function (event) {
	event.preventDefault();

	var id  = $(this).attr('href'),

	top = $(id).offset().top;		

	$('body,html').animate({scrollTop: top}, 1500);
});


});